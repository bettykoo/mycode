#!/usr/bin/python3
"""Alta3 Research | <Betty Koo>
   Using an HTTP GET to determine when the ISS will pass over head"""

# python3 -m pip install requests
import requests

issurl = http://api.open-notify.org/iss-pass.json

def main():
    """your code goes below here"""

    lat = input("what latitude do you want to search?")
    lon = input("what longitude do you want to search?")

    resp = requests.get(f"{issurl}?lat={lat}&lon={lon}")
    print(resp.status_code)


    # stuck? you can always write comments
    # Try describe the steps you would take manually



if __name__ == "__main__":
    main()

